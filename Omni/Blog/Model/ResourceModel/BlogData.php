<?php

namespace Omni\Blog\Model\ResourceModel;

class BlogData extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    protected function _construct()
    {
        $this->_init('omni_blog', 'id');
    }
}