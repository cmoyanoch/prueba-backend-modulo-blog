<?php
namespace Omni\Blog\Model\ResourceModel\BlogData;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{


    protected function _construct()
    {
        $this->_init(
            \Omni\Blog\Model\BlogData::class, 
            \Omni\Blog\Model\ResourceModel\BlogData::class
        );


    }

}