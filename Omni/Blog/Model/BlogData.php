<?php

namespace Omni\Blog\Model;

class BlogData extends \Magento\Framework\Model\AbstractModel 
{
    /**
     * Initialize resource model
     *
     * @return void
     */

    protected function _construct()
    {
        $this->_init(\Omni\Blog\Model\ResourceModel\BlogData::class);
    }

    public function getIdentities()
    {
            return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues()
    {
            $values = [];

            return $values;
    }

}