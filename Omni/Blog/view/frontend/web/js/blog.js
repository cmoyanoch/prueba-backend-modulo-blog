define([
    'jquery', 
    'ko',
    'mage/mage',
    'validation',
    'mage/cookies'
], function ($, ko) {
    'use strict';

    return function (e) {
        $(document).ready(function() {

        $('#blog-form').on('submit', function(event){
           
            event.preventDefault();
            
            var form = $('#blog-form');
            var file_obj = $('#fileimg').get(0).files[0];
            var url = form.attr('action');

          if(form.valid()){
    
                var formData = new FormData(this);
                formData.append('filepath', file_obj);
                formData.append('form_key', $.mage.cookies.get('form_key'));

            $.ajax({
                url: url,
                type: "POST",
                dataType: "JSON",
                contentType: false,
                enctype: 'multipart/form-data',
                processData: false,
                data: formData,
                showLoader: true,
                cache: false,
                beforeSend: function() {
                    $('body').trigger('processStart');
                },
                success: function (response) {
        
                    var data = response['data'];
                    var item ='<div class="item-blog"><div class="item-blog-head"><div class="item-blog-title">';
                        item +='<h2>'+data['title'] +'</h2></div><div class="item-blog-date"><h3>'+data['date']+'</h3>';
                        item +='</div></div><div class="item-blog-info"><div class="item-blog-img"><img src="'+e.dataUrl+'/'+data['file']+'" />';
                        item +='</div><div class="item-blog-comment"><p>'+data['comment']+'</p></div></div></div>';

                        setInterval($('body').trigger('processStop'), 5000);
                        alert('Post entered successfully');
                        $("div .list-blog").prepend(item).show(2000);
                        
                }
            });

            form[0].reset(); 
            return false;


          }


        });

        });
    }
});


