<?php
namespace Omni\Blog\Block;

use Magento\Framework\View\Element\Template;
use Magento\Backend\Block\Template\Context;
use Omni\Blog\Model\BlogDataFactory;


 
class ListBlog extends Template
{

    protected $modelFactory;
    protected $collectionFactory;

    public function __construct(
        Context $context, 
        BlogDataFactory $modelFactory,
        array $data = []
    )
    {
        $this->modelFactory = $modelFactory;
        parent::__construct($context, $data);
    }

    public function getDataCollection()
    {

        $collection = $this->modelFactory->create()->getCollection()->addFieldToSelect('*')->setOrder('id','desc');

         return $collection;
    }

}
