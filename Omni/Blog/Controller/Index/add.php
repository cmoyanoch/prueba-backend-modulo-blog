<?php
namespace Omni\Blog\Controller\Index;

use Magento\Framework\App\Filesystem\DirectoryList;

class Add extends \Magento\Framework\App\Action\Action
{

	protected $logger;
   protected $resultJsonFactory;
	protected $uploaderFactory;
   protected $filesystem;
   protected $bformFactory;
   protected $bformResource;

   protected $collectionFactory;
   protected $formKeyValidator;
   protected $userFactory;

	public function __construct(
		\Magento\Framework\App\Action\Context $context,
		\Psr\Log\LoggerInterface $logger,
      \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
      \Magento\MediaStorage\Model\File\UploaderFactory $uploaderFactory,
      \Magento\Framework\Filesystem $filesystem,
      \Omni\Blog\Model\BlogData $bform,
      \Omni\Blog\Model\ResourceModel\BlogData $bformResource,
      \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator,
      \Magento\User\Model\ResourceModel\User\CollectionFactory $userFactory
	)
	{
		$this->logger = $logger;
      $this->resultJsonFactory = $resultJsonFactory;
      $this->uploaderFactory = $uploaderFactory;
      $this->filesystem = $filesystem;
      $this->bform = $bform;
      $this->bformResource = $bformResource;
      $this->formKeyValidator = $formKeyValidator;
      $this->userFactory = $userFactory;


		return parent::__construct($context);
	}

	public function execute()
	{

      $data = $this->getRequest()->getParams();
      $file = $this->getRequest()->getFiles('filepath');
      $fileName = ($file && array_key_exists('name', $file)) ? $file['name'] : null;
      $form_key =$this->formKeyValidator->validate($this->getRequest()); //validacion de envio de formulario

      $adminUsers = $this->userFactory->create();
      $data['rol'] = '';

      foreach($adminUsers as $adminUser) {
        if($adminUser->getRoleName() === "Administrators" && $adminUser->getEmail() === $data['email'] ){
            $data['rol'] = $adminUser->getRoleName();
        }
   
      }


      if ($data && $form_key) {
            try{

               if ($file && $fileName) {
                   $data['file'] = $this->uploadImage('filepath');
               }
   
               $bformModel= $this->bform;
               $bformModel->setTitle($data['title']);
               $bformModel->setComment($data['comment']);
               $bformModel->setEmail($data['email']);     
               $bformModel->setRol($data['rol']);
               $bformModel->setData('file',$data['file']);

               $this->bformResource->save($bformModel);
               $resultJson = $this->resultJsonFactory->create();
               $data['date'] =  date('d-m-Y');

               $this->logger->info("data " . json_encode($data));
               return $resultJson->setData(['success' => 'true','data' => $data ]);

            } catch (Exception $exception) {
               $this->logger->error($exception->getMessage());
            }
            
      }

      return false;

   }


    public function uploadImage($fileId)
    {

        $uploader = $this->uploaderFactory->create(['fileId' => $fileId]);
        $uploader->setAllowedExtensions(['jpg', 'png', 'jpeg', 'gif']);
        $uploader->setFilesDispersion(false);
        $uploader->setAllowRenameFiles(false);
    
        $path = $this->filesystem->getDirectoryRead(DirectoryList::APP)->getAbsolutePath('code/Omni/Blog/view/frontend/web/images/');
        $result = $uploader->save($path);
        $imagePath = 'images/'.$result['file'];

        return $imagePath;
    }

   
   

}




