<?php
namespace Omni\Blog\Controller\Index;

/**
 * Blog home page view
 */
class Index extends \Magento\Framework\App\Action\Action
{
	protected $_pageFactory;
	private $logger;

	public function __construct(
		\Magento\Framework\App\Action\Context $context,
		\Magento\Framework\View\Result\PageFactory $pageFactory,
		\Psr\Log\LoggerInterface $logger

	)
	{
	
		$this->_pageFactory = $pageFactory;
		$this->logger = $logger;
		return parent::__construct($context);

	}

	public function execute()
	{
		return $this->_pageFactory->create();
    }
}
