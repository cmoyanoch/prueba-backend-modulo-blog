<?php
namespace Omni\Blog\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{

    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        $tableName = $installer->getTable('omni_blog');


        try {

          if (!$installer->tableExists('omni_blog')) {

         $table = $installer->getConnection()->newTable($tableName)
          ->addColumn(
            'id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'nullable' => false, 'primary' => true], 'Blog Id' )
          ->addColumn(
            'title',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, ['nullable' => false], 'Title' )
          ->addColumn(
            'comment',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, ['nullable' => false], 'Comment' )
          ->addColumn(
            'email',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, ['nullable' => false], 'Email Id' )
            ->addColumn(
            'file',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, ['nullable' => false], 'File')
            ->addColumn(
            'created_at',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,  null, ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT], 'Created At')
            ->addColumn(
            'updated_at',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP, null,  ['default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE], 'Updated At' )
            ->addColumn(
              'rol',
              \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, ['nullable' => false], 'Rol' )
            ->addIndex(
              $installer->getIdxName(
                  'omni_blog',
                  ['file','title','comment','email'],
                  \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
              ),
              ['file','title','comment','email'],
              ['type' => \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT]
            )
            ->setComment('Blog Table');

            $installer->getConnection()->createTable($table);

          }
            $installer->endSetup();

        
        } catch (Exception $err) {
            \Magento\Framework\App\ObjectManager::getInstance()->get('Psr\Log\LoggerInterface')->info($err->getMessage());
        }
    
    
    }
}